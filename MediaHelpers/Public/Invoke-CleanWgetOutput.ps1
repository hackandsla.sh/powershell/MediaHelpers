function Invoke-CleanWgetOutput {
    [CmdletBinding(SupportsShouldProcess)]
    param(
        # The path to clean
        [Parameter(Mandatory = $true)]
        [ValidateScript( { Test-ValidPath $_ })]
        [string]
        $Path
    )

    $files = Get-ChildItem -Recurse -File $Path
    $filesToDelete = $files | Where-Object { $_.Extension -eq "" -or $_.Name -match "^.*\.\d$" }
    foreach ($file in $filesToDelete) {
        if ($PSCmdlet.ShouldProcess($file.Name, "Delete")) {
            $file | Remove-Item -Force
        }
    }
}