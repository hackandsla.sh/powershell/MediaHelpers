function Remove-DuplicateMusic {
    [CmdletBinding(SupportsShouldProcess, ConfirmImpact = "high")]
    param (
        # The path to clean
        [Parameter(Mandatory = $true)]
        [ValidateScript( { Test-ValidPath $_ })]
        [string]
        $Path
    )

    begin { }

    process {
        $childrenToRemove = [System.Collections.ArrayList]@()

        $folders = Get-ChildItem -Recurse -Directory $Path
        # Consider album folders the ones that have actual files in them
        $albumFolders = $folders | Where-Object { $_ | Get-ChildItem -File }
        foreach ($folder in $albumFolders) {
            $children = $folder | Get-ChildItem -File
            foreach ($child in $children) {
                try {
                    $tags = Get-Id3Tag -Path $child
                    $diskNum = $tags.Disc
                    $trackNum = $tags.Track
                    $title = $tags.Title
                } catch {
                    Write-Verbose "Could not get tags from $child"
                    continue
                }

                $badNames = @(
                    "{0}-{1:00} {2}" -f $diskNum, $trackNum, $title
                    "{0:00} {1}" -f $trackNum, $title
                )
                $goodName = "{0:00} - {1}" -f $trackNum, $title

                # Condition 1: there are flac and MP3 files together
                if (
                    $child.Extension -eq ".mp3" -and
                    $children.Name -contains "$($child.Basename).flac" -and
                    $PSCmdlet.ShouldProcess($child.FullName)
                ) {
                    Remove-Item -LiteralPath $child.FullName -Force
                    $child.FullName
                } elseif (
                    $child.BaseName -in $badNames -and
                    $children.BaseName -contains $goodName -and
                    $PSCmdlet.ShouldProcess($child.FullName)
                ) {
                    Remove-Item -LiteralPath $child.FullName -Force
                    $child.FullName
                }
            }
        }
    }

    end { }
}