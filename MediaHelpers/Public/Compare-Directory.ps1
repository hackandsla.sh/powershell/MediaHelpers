function Compare-Directory {
    param (
        # The directory to compare
        [Parameter(Mandatory = $true)]
        [string]
        $InputPath,

        # The difference path
        [Parameter(Mandatory = $true)]
        [string]
        $DifferencePath
    )

    Compare-Object (Get-RelativeChildItem $InputPath) (Get-RelativeChildItem $DifferencePath) | Sort-Object RelativePath, Name -Descending
}