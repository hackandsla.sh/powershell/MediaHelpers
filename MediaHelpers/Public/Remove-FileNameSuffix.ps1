function Remove-FileNameSuffix {
    [CmdletBinding(SupportsShouldProcess)]
    param (
        # The path to clean
        [Parameter(Mandatory = $true)]
        [ValidateScript( { Test-ValidPath $_ })]
        [string]
        $Path,

        # The suffix to remove
        [Parameter(Mandatory = $false)]
        [string]
        $Suffix = " (Album Version)"
    )

    begin { }

    process {
        $children = Get-ChildItem -Recurse -File -Path $Path
        foreach ($child in $children) {
            if ($child.BaseName -like "*$Suffix" -and $PSCmdlet.ShouldProcess($child.Name)) {
                $newName = $child.Name -ireplace [regex]::Escape($Suffix), ""
                Move-Item -LiteralPath $child.FullName -Destination (Join-Path $child.Directory $newName)
            }
        }
    }

    end { }
}