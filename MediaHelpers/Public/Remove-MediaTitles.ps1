function Remove-MediaTitles {
    [CmdletBinding(ConfirmImpact = "low", SupportsShouldProcess = $true)]
    param (
        # The path to remove media titles of
        [Parameter(Mandatory = $true)]
        [string]
        [ValidateScript( { Test-ValidPath $_ })]
        $Path
    )

    begin {
    }

    process {
        $files = Get-ChildItem -File -Path $Path -Recurse
        foreach ($file in $files) {
            try {
                $tags = Get-Id3Tag -Path $file
                $title = $tags.Title
                if ($title -and $PSCmdlet.ShouldProcess("Title ${title}")) {
                    Set-Id3Tag  -Path $file -Tags @{Title = $null }
                }
                $comment = $tags.Comment
                if ($comment -and $PSCmdlet.ShouldProcess("Comment ${comment}")) {
                    Set-Id3Tag  -Path $file -Tags @{Comment = $null }
                }
            } catch {
                # Ignore issues with non-media objects
                Write-Verbose "Could not retrieve tags from $($file.FullName)"
            }
        }
    }

    end {
    }
}