function Rename-MusicFiles {
    [CmdletBinding(SupportsShouldProcess)]
    param (
        # The path to remove media titles of
        [Parameter(Mandatory = $true)]
        [string]
        [ValidateScript( { Test-ValidPath $_ })]
        $Path
    )

    begin { }

    process {
        $files = Get-ChildItem $Path -Recurse -File -Exclude "*.jpg","*.png"
        $info = foreach ($file in $files) {
            try {
                $tags = Get-Id3Tag $file
                [PSCustomObject]@{
                    File = $file
                    Tags = $tags
                }
            } catch {
                Write-Information "Can't extract tags from $file"
            }
        }

        # [array]$disks = $info.Tags.Disc | Sort-Object -Unique

        foreach ($file in $info) {
            try {
                $trackNum = $file.Tags.Track
                $diskNum = $file.Tags.Disc
                $diskCount = $file.Tags.DiscCount
                $title = $file.Tags.Title
                $extension = $file.File.Extension
            } catch {
                Write-Verbose "Skipping $file"
                continue
            }

            $newName = "{0:00} - {1}{2}" -f $trackNum, $title, $extension
            $invalidChars = [IO.Path]::GetInvalidFileNameChars() -join ''
            $re = "[{0}]" -f [RegEx]::Escape($invalidChars)
            $newName = $newName -replace $re, "_"

            if ($diskCount -gt 1 -and !([string]$file.File.Directory).Contains("CD$($diskNum)")) {
                $newName = "CD$($diskNum)\$newName"
            }

            $fullNewName = Join-Path $file.File.Directory $newName
            if ($fullNewName -ne $file.File.FullName -and $PSCmdlet.ShouldProcess("$($file.File.Name) => $($fullNewName)")) {
                New-Item $fullNewName -Force
                Move-Item $file.File -Destination $fullNewName -Force
            }
        }
    }

    end { }
}