function Get-RelativeChildItem {
    [CmdletBinding()]
    param(
        # The path to get relative children of
        [Parameter(Mandatory = $true, ValueFromPipeline = $true)]
        [string]
        [ValidateScript( {
                if (Test-Path $_) {
                    return $true
                } else {
                    throw "Error: Path $_ does not exist"
                }
            })]
        $Path
    )

    begin { }

    process {
        $pathLength = $Path.length
        $children = Get-ChildItem $Path -Recurse
        foreach ($child in $children) {
            Add-Member -InputObject $child -MemberType NoteProperty -Name RelativePath -Value $child.FullName.substring($PathLength + 1)
            $child
        }
    }

    end { }
}
