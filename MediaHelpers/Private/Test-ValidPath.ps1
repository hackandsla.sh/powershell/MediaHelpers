function Test-ValidPath {
    param(
        [string]
        $Path
    )
    if (Test-Path $Path) {
        return $true
    } else {
        throw "Error: Path $Path does not exist"
    }
}