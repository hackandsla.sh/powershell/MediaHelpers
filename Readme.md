# MediaHelpers

Some helper functions for managing my media collection

## Next steps

1. Place all publicly-exposed functions into `MediaHelpers\Public`
2. Place all private functions into `MediaHelpers\Private`
